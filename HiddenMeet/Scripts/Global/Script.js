﻿var initializeUserViewModel = function (dontTrigger) {
    return $.get(window.getUserUrl).done(function (data) {
        window.userViewModel = ko.mapping.fromJS(data, null, new UserViewModel());

        if (!isBound(window.userSection)) {
            ko.applyBindings(window.userViewModel, window.userSection);
        } else {
            window.userViewModel.fromJS(data);
        }
        // Lanzamos un evento personalizado después de obtener los permisos de usuario
        if (dontTrigger) return;

        $(document).trigger("hiddenmeet-ready", { "userViewModel": window.userViewModel });
    });
};

$(document).ready(function () {
    initializeUserViewModel();
});

/**
 * Obtiene una petición Ajax con las opciones pasadas.
 * 
 * @param {string} tipo Verbo de la petición.
 * @param {string} url URL del/los recursos.
 * @param {Object} datos Datos a enviar.
 * @param {string} tipoDatos Tipo de los datos.
 * @return {Object} peticion Petición Ajax.
 */
function ajaxRequest(tipo, url, datos, tipoDatos, conCache) {
    var opciones = {
        dataType: tipoDatos || "json",
        contentType: "application/json; charset=utf-8",
        type: tipo || "GET",
        data: datos ? JSON.stringify(datos) : null,
        cache: conCache || false
    };

    var peticion = $.ajax(url, opciones);

    return peticion;
}

var isBound = function (element) {
    return !!ko.dataFor(element);
};
