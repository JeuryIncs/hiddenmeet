﻿var $userVm;

var Index = function () {
   var $listUser = new ListUserViewModel();
    var inicializarFriendsViewModel = function () {
        var request = $.ajax({
            url: Urls.GetAllFriends,
            type: "GET"
        });

        var $data = [];
        request.done(function (data) {

            $.each(data, function (index, usuario) {
                $userVm = new UserViewModel();
                ko.mapping.fromJS(usuario, { }, $userVm);
                $data.push($userVm);
            });

            $listUser.listUser($data);
            ko.applyBindings($listUser, $('#container-friends').get(0));
        });
    };

    return {
        inicializarFriendsViewModel: inicializarFriendsViewModel
    };
}

$(document).on('hiddenmeet-ready', function () {
    var index = new Index();
    index.inicializarFriendsViewModel();
});

var ListUserViewModel = function (user) {
    var self = this;

    self.listUser = ko.observableArray(user);
}
