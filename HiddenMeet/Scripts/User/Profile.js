﻿var Profile = function () {

    this.self = this;
    self.UsuarioViewModel = ko.observable(userViewModel);

    self.PonerPerfil = function (usuarioVm) {

      $.ajax({
            url: window.Urls.UpdatePictureProfile,
            type: "POST",
            data: { IdUserPicture: usuarioVm.IdUserPicture() }
        });

        initializeUserViewModel(true);
    }
}

function buttonEvent() {
    document.getElementById('links').onclick = function (event) {
        event = event || window.event;
        var target = event.target || event.srcElement,
            link = target.src ? target.parentNode : target,
            options = { index: link, event: event },
            links = this.getElementsByTagName('a');
        blueimp.Gallery(links, options);
    };
}

$(document).on('hiddenmeet-ready', function () {
    var profile = new Profile();

    ko.applyBindings(profile, $('#tabProfile').get(0));

    buttonEvent();
});
