﻿var UserViewModel = function () {
    var self = this;

    self.Id = ko.observable();
    self.Name = ko.observable();
    self.LastName = ko.observable();
    self.Estatus = ko.observable();
    self.Gender = ko.observable();
    self.Email = ko.observable();
    self.EmailConfirmed = ko.observable();
    self.PhoneNumber = ko.observable();
    self.PhoneNumberConfirmed = ko.observable();
    self.UserName = ko.observable();
    self.ImageProfile = ko.observable();
    self.UserPictures = ko.observable(new UserPictureViewModel);

    self.$ImageProfile = ko.computed(function() {
        if (self.ImageProfile() == null) {
            if (self.Gender() === 1) {
                self.ImageProfile("no-profile-pic-m.png");
            } else {
                self.ImageProfile("photo.jpg");
            }
        }
    });

    self.fromJS = function(data) {
        self.Id(data.Id);
        self.Name(data.Name);
        self.LastName(data.LastName);
        self.ImageProfile(data.ImageProfile);
        self.ImageProfile.valueHasMutated();
    };
}
