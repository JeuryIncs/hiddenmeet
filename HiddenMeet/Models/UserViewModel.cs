﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HiddenMeet.Models
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Estatus { get; set; }
        public int? Gender { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PhoneNumber { get; set; }
        public bool LockoutEnabled { get; set; }
        public int? AccessFailedCount { get; set; }
        public string UserName { get; set; }
        public string ImageProfile { get; set; }

        public IEnumerable<UserPictureViewModel> UserPictures { get; set; }

    }
}