﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HiddenMeet.Models
{
    public class UserPictureViewModel
    {
        public int IdUserPicture { get; set; }
        public string IdUser { get; set; }
        public bool IsProfilePicture { get; set; }
        public string Name { get; set; }
    }
}