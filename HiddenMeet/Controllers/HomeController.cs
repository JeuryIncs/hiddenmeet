﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HiddenMeet.Application.Services;
using HiddenMeet.Filters;

namespace HiddenMeet.Controllers
{
    [SecurityFilter]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}