﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using HiddenMeet.Application.Services;
using HiddenMeet.Models;
using Microsoft.AspNet.Identity;

namespace HiddenMeet.Controllers
{
    [Authorize]
    public class IndexController : BaseController
    {
        private readonly IUserService _userService;

        public IndexController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetAllUsers()
        {
            var users = _userService.GetAllUsers(User.Identity.GetUserId());

            var userVm = Mapper.Map<List<UserViewModel>>(users);

            return Json(userVm, JsonRequestBehavior.AllowGet);
        }
    }
}