﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HiddenMeet.Infrastructure;
using Microsoft.AspNet.Identity;

namespace HiddenMeet.Controllers
{
    public class ProfileController : BaseController
    {
        private readonly UserPictureService _userPictureService;
        
        // GET: Profile
        public ActionResult Index()
        {
            return View();
        }

        public ProfileController()
        {
            _userPictureService = new UserPictureService();

        }

        [HttpGet]
        public new ActionResult Profile()
        {
            return View();
        }

        public JsonResult UpdatePictureProfile(int idUserPicture)
        {
            _userPictureService.UpdateUserPicture(idUserPicture, User.Identity.GetUserId());

            UpdateUser();

            return Json(null);
        }
    }
}