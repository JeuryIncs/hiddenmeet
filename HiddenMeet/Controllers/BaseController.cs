﻿using System.Web.Mvc;
using AutoMapper;
using HiddenMeet.Application.Domain;
using HiddenMeet.Application.Services;
using HiddenMeet.Models;
using Microsoft.AspNet.Identity;
using Ninject;

namespace HiddenMeet.Controllers
{
    public class BaseController : Controller
    {
        [Inject]
        public IUserService UserService { get; set; }

        [HttpGet]
        public JsonResult GetCurrentUser()
        {
            if (Session["User"] != null)
            {
                return Json((UserViewModel)Session["User"], JsonRequestBehavior.AllowGet);
            }

            var user = UserService.GetCurrentUser(User.Identity.GetUserId());

            var userVm = Mapper.Map<UserViewModel>(user);

            Session["User"] = userVm;

            return Json((UserViewModel)Session["User"], JsonRequestBehavior.AllowGet);
        }

        protected void UpdateUser()
        {
            var user = UserService.GetCurrentUser(User.Identity.GetUserId());

            var userVm = Mapper.Map<UserViewModel>(user);

            Session["User"] = userVm;
        }

    }
}