﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using HiddenMeet.Controllers;

namespace HiddenMeet.Filters
{
    public class SecurityFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (HttpContext.Current.Request.IsAuthenticated)
            {
                filterContext.Result = new RedirectResult(WebConfigurationManager.AppSettings["DefaultUrl"]);
            }
        }
    }
}