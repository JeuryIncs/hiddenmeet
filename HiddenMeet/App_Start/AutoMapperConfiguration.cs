﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using HiddenMeet.Application.Domain;
using HiddenMeet.Models;

namespace HiddenMeet
{
    public static  class AutoMapperConfiguration
    {

        public static void Configure()
        {
            Mapper.CreateMap<User, UserViewModel>().ForMember(x=> x.ImageProfile,
                tr => tr.MapFrom(x=> x.UserPictures.FirstOrDefault(y => y.IsProfilePicture).Name));

            Mapper.CreateMap<UserPicture, UserPictureViewModel>();

        }
    }
}