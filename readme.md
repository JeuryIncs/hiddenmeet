﻿# Archivo de changelog para tracking de las funcionalidades y actividades del sistema.

Por cada versión del sistema nueva se listarán aquí los cambios que se hicieron detalladamente.
Este archivo está escrito en formato `markdown`, [aquí](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
hay referencias acerca de esta sintaxis.

## Versión 0.0.0.1

### Feature

* Se agrega el readme para llevar un changelog de los cambios del aplicativo.
