﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiddenMeet.Infrastructure
{
    interface IService<TEntity> where TEntity : class
    {
        IDbContext dbContext { get; set; }
        IRepository<TEntity> repository { get; set; }
        void Insert(TEntity model);
        void Update(TEntity model);
        IEnumerable<TEntity> GetAll();

    }
}
