using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using HiddenMeet.Application.Domain;
using HiddenMeet.Infrastructure.Models.Mapping;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HiddenMeet.Infrastructure.Models
{
    public partial class HiddenMeetContext : IdentityDbContext<User>, IDbContext
    {
        static HiddenMeetContext()
        {
            Database.SetInitializer<HiddenMeetContext>(null);
        }

        public HiddenMeetContext()
            : base("HiddenMeet")
        {
        }

        public DbSet<Favorite> Favorites { get; set; }
        public DbSet<Mesage> Mesages { get; set; }
        public DbSet<UserPicture> UserPictures { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<IdentityUserRole>()
            .HasKey(r => new { r.UserId, r.RoleId })
            .ToTable("UserRole");

            modelBuilder.Entity<IdentityUserLogin>()
                .HasKey(l => new
                {
                    l.LoginProvider,
                    l.ProviderKey,
                    l.UserId
                })
                .ToTable("UserLogin");

            modelBuilder.Entity<IdentityUserClaim>()
               .ToTable("UserClaim")
               .Property(p => p.Id).HasColumnName("UserClaimId");

            modelBuilder.Entity<IdentityRole>()
             .ToTable("Roles")
             .Property(p => p.Id).HasColumnName("RoleId");
            
            modelBuilder.Configurations.Add(new FavoriteMap());
            modelBuilder.Configurations.Add(new MesageMap());
            modelBuilder.Configurations.Add(new UserPictureMap());
            modelBuilder.Configurations.Add(new UserProfileMap());
            modelBuilder.Configurations.Add(new UserMap());
        }

        public static HiddenMeetContext Create()
        {
            return new HiddenMeetContext();
        }

        public int SaveChanges(bool guardarAuditoria)
        {
            return guardarAuditoria ? SaveChanges() : base.SaveChanges();
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

    }
}
