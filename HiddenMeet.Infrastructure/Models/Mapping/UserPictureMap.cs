using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using HiddenMeet.Application.Domain;

namespace HiddenMeet.Infrastructure.Models.Mapping
{
    public class UserPictureMap : EntityTypeConfiguration<UserPicture>
    {
        public UserPictureMap()
        {
            // Primary Key
            this.HasKey(t => t.IdUserPicture);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(70);

            // Table & Column Mappings
            this.ToTable("UserPicture");
            this.Property(t => t.IdUserPicture).HasColumnName("IdUserPicture");
            this.Property(t => t.IdUser).HasColumnName("IdUser");
            this.Property(t => t.IsProfilePicture).HasColumnName("IsProfilePicture");
            this.Property(t => t.Name).HasColumnName("Name");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.UserPictures)
                .HasForeignKey(d => d.IdUser);

        }
    }
}
