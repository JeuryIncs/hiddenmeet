using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using HiddenMeet.Application.Domain;

namespace HiddenMeet.Infrastructure.Models.Mapping
{
    public class MesageMap : EntityTypeConfiguration<Mesage>
    {
        public MesageMap()
        {
            // Primary Key
            this.HasKey(t => t.IdMessage);

            // Properties
            // Table & Column Mappings
            this.ToTable("Mesage");
            this.Property(t => t.IdMessage).HasColumnName("IdMessage");
            this.Property(t => t.IdUserSend).HasColumnName("IdUserSend");
            this.Property(t => t.IdUserRecipient).HasColumnName("IdUserRecipient");
            this.Property(t => t.DateSend).HasColumnName("DateSend");
            this.Property(t => t.DateRead).HasColumnName("DateRead");
            this.Property(t => t.IsRead).HasColumnName("IsRead");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.Mesages)
                .HasForeignKey(d => d.IdUserRecipient);
            this.HasRequired(t => t.User1)
                .WithMany(t => t.Mesages1)
                .HasForeignKey(d => d.IdUserSend);

        }
    }
}
