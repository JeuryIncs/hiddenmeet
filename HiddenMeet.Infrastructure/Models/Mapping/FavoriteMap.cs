using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using HiddenMeet.Application.Domain;

namespace HiddenMeet.Infrastructure.Models.Mapping
{
    public class FavoriteMap : EntityTypeConfiguration<Favorite>
    {
        public FavoriteMap()
        {
            // Primary Key
            this.HasKey(t => t.IdFavorite);

            // Properties
            // Table & Column Mappings
            this.ToTable("Favorite");
            this.Property(t => t.IdFavorite).HasColumnName("IdFavorite");
            this.Property(t => t.IdUser).HasColumnName("IdUser");
            this.Property(t => t.IdUserFavorite).HasColumnName("IdUserFavorite");
             

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.Favorites)
                .HasForeignKey(d => d.IdUser);
            this.HasRequired(t => t.User1)
                .WithMany(t => t.Favorites1)
                .HasForeignKey(d => d.IdUserFavorite);

        }
    }
}
