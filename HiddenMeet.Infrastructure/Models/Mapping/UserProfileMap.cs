using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using HiddenMeet.Application.Domain;

namespace HiddenMeet.Infrastructure.Models.Mapping
{
    public class UserProfileMap : EntityTypeConfiguration<UserProfile>
    {
        public UserProfileMap()
        {
            // Primary Key
            this.HasKey(t => t.IdUserProfile);

            // Properties
            this.Property(t => t.AboutMe)
                .HasMaxLength(500);

            this.Property(t => t.Phone)
                .HasMaxLength(100);

            this.Property(t => t.Country)
                .HasMaxLength(70);

            this.Property(t => t.Adress)
                .HasMaxLength(70);

            this.Property(t => t.Languages)
                .HasMaxLength(70);

            this.Property(t => t.Interest)
                .HasMaxLength(70);

            this.Property(t => t.CivilStatus)
                .HasMaxLength(70);

            this.Property(t => t.Education)
                .HasMaxLength(70);

            this.Property(t => t.EyesColor)
                .HasMaxLength(70);

            this.Property(t => t.FavoriteMusic)
                .HasMaxLength(70);

            this.Property(t => t.FavoriteTvShow)
                .HasMaxLength(70);

            this.Property(t => t.FavoriteMovie)
                .HasMaxLength(70);

            this.Property(t => t.FavoriteBook)
                .HasMaxLength(70);

            // Table & Column Mappings
            this.ToTable("UserProfile");
            this.Property(t => t.IdUserProfile).HasColumnName("IdUserProfile");
            this.Property(t => t.IdUser).HasColumnName("IdUser");
            this.Property(t => t.Gender).HasColumnName("Gender");
            this.Property(t => t.AboutMe).HasColumnName("AboutMe");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Country).HasColumnName("Country");
            this.Property(t => t.Adress).HasColumnName("Adress");
            this.Property(t => t.Languages).HasColumnName("Languages");
            this.Property(t => t.Interest).HasColumnName("Interest");
            this.Property(t => t.CivilStatus).HasColumnName("CivilStatus");
            this.Property(t => t.Education).HasColumnName("Education");
            this.Property(t => t.EyesColor).HasColumnName("EyesColor");
            this.Property(t => t.Height).HasColumnName("Height");
            this.Property(t => t.FavoriteMusic).HasColumnName("FavoriteMusic");
            this.Property(t => t.FavoriteTvShow).HasColumnName("FavoriteTvShow");
            this.Property(t => t.FavoriteMovie).HasColumnName("FavoriteMovie");
            this.Property(t => t.FavoriteBook).HasColumnName("FavoriteBook");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.UserProfiles)
                .HasForeignKey(d => d.IdUser);

        }
    }
}
