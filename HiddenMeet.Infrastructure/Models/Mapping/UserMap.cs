using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using HiddenMeet.Application.Domain;

namespace HiddenMeet.Infrastructure.Models.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.UserName)
                .HasMaxLength(70);

            this.Property(t => t.Name)
                .HasMaxLength(70);

            this.Property(t => t.Email)
                .HasMaxLength(100);

            this.Property(t => t.LastName)
                .HasMaxLength(70);

            this.Property(t => t.Passwords)
                .HasMaxLength(70);

            this.Property(t => t.Phone)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("Users");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Passwords).HasColumnName("Passwords");
            this.Property(t => t.Estatus).HasColumnName("Estatus");
            this.Property(t => t.Gender).HasColumnName("Gender");
            this.Property(t => t.Phone).HasColumnName("Phone");
        }
    }
}
