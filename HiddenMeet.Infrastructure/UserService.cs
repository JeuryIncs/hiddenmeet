﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using HiddenMeet.Application.Domain;
using HiddenMeet.Application.Services;

namespace HiddenMeet.Infrastructure
{
    public class UserService : BaseService<User>, IUserService
    {
        public  IEnumerable<User> GetAllUsers(string idUser)
        {
            return repository.GetAll()
                .Where(x=> x.Id != idUser)
                .Include(x=> x.UserPictures).ToList();
        }

        public User GetUserById(string id)
        {
            return repository.GetAll()
                .FirstOrDefault(x => x.Id == id);
        }

        public User GetCurrentUser(string id)
        {
            return repository.GetAll()
                .Include(x => x.UserPictures)
                .Where(x=> x.UserPictures.Any(a=> a.IsProfilePicture))
                .FirstOrDefault(x => x.Id == id);
        }

        public User GetUserDetailsById(string id)
        {
            return repository.GetAll()
                .Include(x => x.UserPictures)
                .Include(x => x.UserProfiles)
                .Where(x => x.UserPictures.Any(a=> a.IsProfilePicture))
                .FirstOrDefault(x => x.Id == id);
        }
    }
}


