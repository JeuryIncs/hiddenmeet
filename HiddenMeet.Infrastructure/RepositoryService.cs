﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HiddenMeet.Infrastructure
{
    public class RepositoryService<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private IDbContext Context;

        private IDbSet<TEntity> Entities
        {
            get { return this.Context.Set<TEntity>(); }
        }

        public RepositoryService(IDbContext context)
        {
            this.Context = context;
        }

        public IQueryable<TEntity> GetAll()
        {
            return Entities.AsQueryable();
        }

        public TEntity GetById(object id)
        {
            return Entities.Find(id);
        }

        public void Insert(TEntity entity)
        {
            Entities.Add(entity);
        }

        public void Update(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            this.Context.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            Entities.Remove(entity);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.Context != null)
                {
                    this.Context.Dispose();
                    this.Context = null;
                }
            }
        }

        public IQueryable<TEntity> Select(System.Linq.Expressions.Expression<Func<TEntity, bool>> query)
        {
            return this.Context.Set<TEntity>().Where(query);
        }

        // TODO :AAM: Implementar tomar el query de un sitio antes de hacer los include en todos los métodos.

        /// <summary>
        /// Incluye la entidad dada en el objeto que retorna la consulta
        /// </summary>
        /// <param name="expresionInclude">Lambda que representa la entidad a incluir</param>
        /// <returns></returns>
        public IQueryable<TEntity> Include(Expression<Func<TEntity, object>> expresionInclude)
        {
            return Context.Set<TEntity>().Include(expresionInclude);
        }

        /// <summary>
        /// Incluye la entidad dada en el objeto que retorna la consulta
        /// </summary>
        /// <param name="nombreEntidad">Nombre de la entidad a incluir</param>
        /// <returns></returns>
        public IQueryable<TEntity> Include(string nombreEntidad)
        {
            return Context.Set<TEntity>().Include(nombreEntidad);
        }
    }
}
