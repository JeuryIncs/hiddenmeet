﻿using System;
using System.Linq;
using HiddenMeet.Application.Domain;
using HiddenMeet.Application.Services;

namespace HiddenMeet.Infrastructure
{
    public class UserPictureService : BaseService<UserPicture>, IUserPicture
    {
        public void UpdateUserPicture(int idPicture, string idUser)
        {
            ValidatePicture(idPicture);

            UserPictureFalse(idUser);

            UserPictureTrue(idPicture);
        }

        private void ValidatePicture(int idPicture)
        {
            var picture = repository.GetById(idPicture);

            if (picture == null)
            {
                throw new NullReferenceException("No existe la foto a editar.");
            }
        }

        private void UserPictureFalse(string id)
        {
            var userPictures = repository.GetAll().Where(x => x.IdUser == id);

            foreach (var picture in userPictures)
            {
                picture.IsProfilePicture = false;
            }
        }

        private void UserPictureTrue(int idPicture)
        {
            var userPictures = repository.GetAll().FirstOrDefault(x => x.IdUserPicture == idPicture);

            if (userPictures != null) userPictures.IsProfilePicture = true;

            dbContext.SaveChanges();
        }
    }
}
