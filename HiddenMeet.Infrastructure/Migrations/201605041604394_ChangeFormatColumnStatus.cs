namespace HiddenMeet.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeFormatColumnStatus : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Estatus", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Estatus", c => c.Int());
        }
    }
}
