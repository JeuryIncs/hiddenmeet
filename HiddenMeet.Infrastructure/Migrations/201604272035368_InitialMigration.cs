namespace HiddenMeet.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Favorite",
                c => new
                    {
                        IdFavorite = c.Int(nullable: false, identity: true),
                        IdUser = c.String(nullable: false, maxLength: 128),
                        IdUserFavorite = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.IdFavorite)
                .ForeignKey("dbo.Users", t => t.IdUser, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.IdUserFavorite, cascadeDelete: false)
                .Index(t => t.IdUser)
                .Index(t => t.IdUserFavorite);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(maxLength: 70),
                        LastName = c.String(maxLength: 70),
                        Passwords = c.String(maxLength: 70),
                        Estatus = c.Int(),
                        Gender = c.Int(),
                        Phone = c.String(maxLength: 30),
                        Email = c.String(maxLength: 100),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(maxLength: 70),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserClaim",
                c => new
                    {
                        UserClaimId = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.UserClaimId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserLogin",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Mesage",
                c => new
                    {
                        IdMessage = c.Int(nullable: false, identity: true),
                        IdUserSend = c.String(nullable: false, maxLength: 128),
                        IdUserRecipient = c.String(nullable: false, maxLength: 128),
                        DateSend = c.DateTime(nullable: false),
                        DateRead = c.DateTime(),
                        IsRead = c.Boolean(),
                    })
                .PrimaryKey(t => t.IdMessage)
                .ForeignKey("dbo.Users", t => t.IdUserRecipient, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.IdUserSend, cascadeDelete: false)
                .Index(t => t.IdUserSend)
                .Index(t => t.IdUserRecipient);
            
            CreateTable(
                "dbo.UserRole",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.UserPicture",
                c => new
                    {
                        IdUserPicture = c.Int(nullable: false, identity: true),
                        IdUser = c.String(nullable: false, maxLength: 128),
                        IsProfilePicture = c.Boolean(nullable: false),
                        Name = c.String(maxLength: 70),
                    })
                .PrimaryKey(t => t.IdUserPicture)
                .ForeignKey("dbo.Users", t => t.IdUser, cascadeDelete: true)
                .Index(t => t.IdUser);
            
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        IdUserProfile = c.Int(nullable: false, identity: true),
                        IdUser = c.String(nullable: false, maxLength: 128),
                        Gender = c.Boolean(),
                        AboutMe = c.String(maxLength: 500),
                        Phone = c.String(maxLength: 100),
                        Country = c.String(maxLength: 70),
                        Adress = c.String(maxLength: 70),
                        Languages = c.String(maxLength: 70),
                        Interest = c.String(maxLength: 70),
                        CivilStatus = c.String(maxLength: 70),
                        Education = c.String(maxLength: 70),
                        EyesColor = c.String(maxLength: 70),
                        Height = c.Decimal(precision: 18, scale: 2),
                        FavoriteMusic = c.String(maxLength: 70),
                        FavoriteTvShow = c.String(maxLength: 70),
                        FavoriteMovie = c.String(maxLength: 70),
                        FavoriteBook = c.String(maxLength: 70),
                    })
                .PrimaryKey(t => t.IdUserProfile)
                .ForeignKey("dbo.Users", t => t.IdUser, cascadeDelete: true)
                .Index(t => t.IdUser);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleId = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.RoleId)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRole", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.Favorite", "IdUserFavorite", "dbo.Users");
            DropForeignKey("dbo.Favorite", "IdUser", "dbo.Users");
            DropForeignKey("dbo.UserProfile", "IdUser", "dbo.Users");
            DropForeignKey("dbo.UserPicture", "IdUser", "dbo.Users");
            DropForeignKey("dbo.UserRole", "UserId", "dbo.Users");
            DropForeignKey("dbo.Mesage", "IdUserSend", "dbo.Users");
            DropForeignKey("dbo.Mesage", "IdUserRecipient", "dbo.Users");
            DropForeignKey("dbo.UserLogin", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserClaim", "UserId", "dbo.Users");
            DropIndex("dbo.Roles", "RoleNameIndex");
            DropIndex("dbo.UserProfile", new[] { "IdUser" });
            DropIndex("dbo.UserPicture", new[] { "IdUser" });
            DropIndex("dbo.UserRole", new[] { "RoleId" });
            DropIndex("dbo.UserRole", new[] { "UserId" });
            DropIndex("dbo.Mesage", new[] { "IdUserRecipient" });
            DropIndex("dbo.Mesage", new[] { "IdUserSend" });
            DropIndex("dbo.UserLogin", new[] { "UserId" });
            DropIndex("dbo.UserClaim", new[] { "UserId" });
            DropIndex("dbo.Favorite", new[] { "IdUserFavorite" });
            DropIndex("dbo.Favorite", new[] { "IdUser" });
            DropTable("dbo.Roles");
            DropTable("dbo.UserProfile");
            DropTable("dbo.UserPicture");
            DropTable("dbo.UserRole");
            DropTable("dbo.Mesage");
            DropTable("dbo.UserLogin");
            DropTable("dbo.UserClaim");
            DropTable("dbo.Users");
            DropTable("dbo.Favorite");
        }
    }
}
