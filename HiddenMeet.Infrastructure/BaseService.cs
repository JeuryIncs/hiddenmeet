﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using HiddenMeet.Infrastructure.Models;

namespace HiddenMeet.Infrastructure
{
    public abstract class BaseService<T> : IService<T> where T : class
    {
        public IDbContext dbContext { get; set; }
        public IRepository<T> repository { get; set; }

        protected BaseService()
        {
            dbContext = new HiddenMeetContext();
            repository = new RepositoryService<T>(dbContext);
        }

        #region Metodos Comunes

        public virtual void Insert(T model)
        {
            repository.Insert(model);
            dbContext.SaveChanges();
        }

        public void Insert(bool guardarAuditoria, T model)
        {
            repository.Insert(model);
            dbContext.SaveChanges(guardarAuditoria);
        }

        protected virtual void Insert(T model, bool salvar)
        {
            repository.Insert(model);
            if (salvar) dbContext.SaveChanges();
        }
        
        public virtual void Update(T model)
        {
            repository.Update(model);
            dbContext.SaveChanges();
        }

        public void Update(bool guardarCambios, T modelo)
        {
            dbContext.Entry(modelo).State = EntityState.Modified;

            if (guardarCambios) dbContext.SaveChanges();
        }

        public void SetValues(Object obj, T model)
        {
            dbContext.Entry(obj).CurrentValues.SetValues(model);
        }

        protected virtual void Update(T model, bool salvar)
        {
            repository.Update(model);
            if (salvar) dbContext.SaveChanges();
        }

        public void Delete(T model)
        {
            repository.Delete(model);
            dbContext.SaveChanges();
        }

        public void Delete(T model, bool salvar)
        {
            repository.Delete(model);
            if (salvar) dbContext.SaveChanges();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return repository.GetAll();
        }

        public IQueryable<T> Select(Expression<Func<T, bool>> query)
        {
            return repository.Select(query);
        }
        
        public T GetById(object id)
        {
            return repository.GetById(id);
        }

        protected int SaveChanges()
        {
            return dbContext.SaveChanges();
        }
        
        #endregion

    }
}
