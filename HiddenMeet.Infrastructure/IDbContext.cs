﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiddenMeet.Infrastructure
{
    public interface IDbContext
    {
        IDbSet<TEntity> Set<TEntity>() where TEntity : class;
        int SaveChanges();
        int SaveChanges(bool guardarAuditoria);
        void Dispose();
        System.Data.Entity.Infrastructure.DbEntityEntry Entry(object entity);
    }
}
