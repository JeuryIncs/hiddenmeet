using System;
using System.Collections.Generic;

namespace HiddenMeet.Application.Domain
{
    public partial class Mesage
    {
        public int IdMessage { get; set; }
        public string IdUserSend { get; set; }
        public string IdUserRecipient { get; set; }
        public System.DateTime DateSend { get; set; }
        public Nullable<System.DateTime> DateRead { get; set; }
        public Nullable<bool> IsRead { get; set; }
        public virtual User User { get; set; }
        public virtual User User1 { get; set; }
    }
}
