﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiddenMeet.Application.Domain
{
    public enum GenderEnum
    {
        Masculino = 1,
        Femenino = 2
    }
}
