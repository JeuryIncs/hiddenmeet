using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HiddenMeet.Application.Domain
{
    public partial class User : IdentityUser
    {
        public User()
        {
            this.Favorites = new List<Favorite>();
            this.Favorites1 = new List<Favorite>();
            this.Mesages = new List<Mesage>();
            this.Mesages1 = new List<Mesage>();
            this.UserPictures = new List<UserPicture>();
            this.UserProfiles = new List<UserProfile>();
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }
        
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Passwords { get; set; }
        public string Estatus { get; set; }
        public int? Gender { get; set; }
        public string Phone { get; set; }
        public virtual ICollection<Favorite> Favorites { get; set; }
        public virtual ICollection<Favorite> Favorites1 { get; set; }
        public virtual ICollection<Mesage> Mesages { get; set; }
        public virtual ICollection<Mesage> Mesages1 { get; set; }
        public virtual ICollection<UserPicture> UserPictures { get; set; }
        public virtual ICollection<UserProfile> UserProfiles { get; set; }
    }
}
