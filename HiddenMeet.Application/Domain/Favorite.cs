using System;
using System.Collections.Generic;

namespace HiddenMeet.Application.Domain
{
    public partial class Favorite
    {
        public int IdFavorite { get; set; }
        public string IdUser { get; set; }
        public string IdUserFavorite { get; set; }

        public virtual User User { get; set; }
        public virtual User User1 { get; set; }
    }
}
