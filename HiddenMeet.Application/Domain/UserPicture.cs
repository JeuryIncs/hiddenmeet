using System;
using System.Collections.Generic;

namespace HiddenMeet.Application.Domain
{
    public partial class UserPicture
    {
        public int IdUserPicture { get; set; }
        public string IdUser { get; set; }
        public bool IsProfilePicture { get; set; }
        public string Name { get; set; }
        public virtual User User { get; set; }
    }
}
