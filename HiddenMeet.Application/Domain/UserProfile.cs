using System;
using System.Collections.Generic;

namespace HiddenMeet.Application.Domain
{
    public partial class UserProfile
    {
        public int IdUserProfile { get; set; }
        public string IdUser { get; set; }
        public Nullable<bool> Gender { get; set; }
        public string AboutMe { get; set; }
        public string Phone { get; set; }
        public string Country { get; set; }
        public string Adress { get; set; }
        public string Languages { get; set; }
        public string Interest { get; set; }
        public string CivilStatus { get; set; }
        public string Education { get; set; }
        public string EyesColor { get; set; }
        public Nullable<decimal> Height { get; set; }
        public string FavoriteMusic { get; set; }
        public string FavoriteTvShow { get; set; }
        public string FavoriteMovie { get; set; }
        public string FavoriteBook { get; set; }
        public virtual User User { get; set; }
    }
}
