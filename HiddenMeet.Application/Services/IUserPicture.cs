﻿using HiddenMeet.Application.Domain;

namespace HiddenMeet.Application.Services
{
    public interface IUserPicture
    {
        void UpdateUserPicture(int idPicture, string idUser);
    }
}
