﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HiddenMeet.Application.Domain;

namespace HiddenMeet.Application.Services
{
    public interface IUserService
    {
        IEnumerable<User> GetAllUsers(string id);
        User GetUserById(string id);
        User GetCurrentUser(string id);
        User GetUserDetailsById(string id);
    }
}
